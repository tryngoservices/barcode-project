<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Validator;
use App\Models\VehicleStatus;
use App\Models\UserSession;
use App\Models\UserLogs;
use App\Models\Users;
use App\Models\TokenCode;
use App\Models\EmailContent;
use App\Models\ConnectionDetails;
use App\Models\EncryptionParams;
use Mail;
//use App\Functions\ripcord;

class AdminController extends Controller
{
	public function changeUserDetail(Request $request){
		if($request->IsMethod("post")){
			// $user = Users::find($request->user_id);
			// $user->first_name = $request->first_name;
			// $user->last_name = $request->last_name;
			// $user->save();
			
            // return $this->resp(1,"User Data Updated!",['user'=>$user]);
		}
	}
    public function getRecord(Request $request){
        if(isset($request->value)){
        	// include(app_path().'/Functions/testapi.php');
            include(app_path().'/Functions/newchange.php');
        	
            $value = $request->value;
	    #$url = $this->getConnectionDetails("host_url");
	    	$url = "http://172.20.20.10:8082";
    		$db = $this->getConnectionDetails("db");
    		/*$url = "https://trackrssl.topc.ph";
    		$db = "custom";*/
    		$username = $this->getConnectionDetails("username");
    		$password = $this->getConnectionDetails("password");
		//$password = "test1234";
        	$value = fetchRecord($value,$url,$db,$username,$password);
        	if(isset($value['faultCode'])){
        		return $this->resp(0,"Record Not Found",NULL);
    		}
        	return $this->resp(1,"Record Found",$value);
        }
        else{
            return $this->resp(0,"Field Empty",NULL);
        }
    }

    public function getClient(Request $request){
    	if(!isset($request->value)){
    		return $this->resp(0,'Field Empty.',NULL);
    	}
        $value = $request->value;

        	// include(app_path().'/Functions/testapi.php');
            include(app_path().'/Functions/newchange.php');
        	
            // $value = $request->value;
	        $url = "http://172.20.20.10:8082";
	        //$url = $this->getConnectionDetails("host_url");
    		$db = $this->getConnectionDetails("db");
    		/*$url = "https://trackrssl.topc.ph";
    		$db = "custom";*/
    		$username = $this->getConnectionDetails("username");
    		$password = $this->getConnectionDetails("password");
		//$password = "test1234";

        	$value = fetchRecord($value,$url,$db,$username,$password);
        	if(isset($value['faultCode'])){
        		return $this->resp(0,"Record Not Found",NULL);
    		}
        	return $this->resp(1,"Record Found",$value);

		// Below Lines commented on 4-10-2021
        // include(app_path().'/Functions/client.php');
    	 
		// $url = $this->getConnectionDetails("host_url");
		// $db = $this->getConnectionDetails("db");
		// /*$url = "https://trackrssl.topc.ph";
		// $db = "custom";*/
		// $username = $this->getConnectionDetails("username");
		// $password = $this->getConnectionDetails("password");

    	// //$value = clientPHP($value,$url,$db,$username,$password);
        // $value = clientPHP($value,$url,$db,$username,$password);
        // if(isset($value['faultCode'])){
    	// 	return $this->resp(0,"Record Not Found",NULL);
		// }
		// if($value){
    	// 	return $this->resp(1,"Record Found",$value);
		// }
		// else{
    	// 	return $this->resp(0,"Record Empty",NULL);
		// }
		// Line comment ends - 4-10-2021
    }

    public function makeScanHistoryInsert(Request $request){

    	include(app_path().'/Functions/scanhistory.php');
    	        $url = "http://172.20.20.10:8082";
		//$url = $this->getConnectionDetails("host_url");
		$db = $this->getConnectionDetails("db");
		/*$url = "https://trackrssl.topc.ph";
		$db = "custom";*/
		$username = $this->getConnectionDetails("username");
		$password = $this->getConnectionDetails("password");
		$data = $request->all();
		//$password = "test1234";
    	$value = insertScanHistory($data,$url,$db,$username,$password);
		if($value){
	    	if($value == 'error'){
	    		return $this->resp(0,"Required Fields Empty",NULL);
			}
    		return $this->resp(1,"Record Created",$value);
		}
		else{
    		return $this->resp(0,"Record Creation Failed",NULL);
		}
    }


    public function makeScanReport(Request $request){

    	include(app_path().'/Functions/report.php');
    	        $url = "http://172.20.20.10:8082";
		//$url = $this->getConnectionDetails("host_url");
		$db = $this->getConnectionDetails("db");
		/*$url = "https://trackrssl.topc.ph";
		$db = "custom";*/
		$username = $this->getConnectionDetails("username");
		$password = $this->getConnectionDetails("password");
		$csvpath = $this->getConnectionDetails("csv_path_absolute");
		$data = $request->ids;

    	$value = makeReportofIds($data,$url,$db,$username,$password);
		if($value){
  			$valuArr = $this->getUserEmail($value[0]);
  			if($valuArr){
			  	$fileName = $valuArr['first_name'].$valuArr['last_name'].date('Y-m-d-H-i-s').'.csv';
			  	$fileName = str_replace(' ', '-', $fileName);

			  	$this->create_csv_string($value,$fileName);

				$staticurl = $csvpath.'/REPORTS/'.$fileName;
				$emailTemplate = $this->getEmailContent("send_report");

                //$msg = $emailTemplate->content.$staticurl;
                $msg = $emailTemplate->content;

				$msg = wordwrap($msg,70);

		        $data = ['messages' => $msg,'url'=>$staticurl];
		        $email = $valuArr['email'];
		        $checkError = Mail::send('report', $data, function($message) use($email,$emailTemplate) {
		         $message->to($email, 'Report')->subject
		            ($emailTemplate->subject);
                 $message->from('emailer@irsis.ph','Trackrmobile');
		        });
		        return $this->resp(1,"Email Sent",NULL);
  			}
  			else{
	    		return $this->resp(0,"User Record Not Found",NULL);
			}
		}
		else{
    		return $this->resp(0,"Record Fetch Failed",NULL);
		}
    }

    public function updateConnectionFeature(Request $request)
    {
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$validator = Validator::make($request->all(), [
                'host' => 'required',
                'db' => 'required',
                'username' => 'required',
                'password' => 'required'
	        ]);
	        if ($validator->fails()) {
	            return $this->resp(0,"Fill the required fields!",['error' => 'Fill the required fields!']);
	        }
    		
    		$this->setConnectionDetails("host_url",$request->host);
    		$this->setConnectionDetails("db",$request->db);
    		$this->setConnectionDetails("username",$request->username);
            $this->setConnectionDetails("password",$request->password);
            $this->setConnectionDetails("csv_path_absolute",$request->csv_path);

	        return $this->resp(1,"Connection Details Updated Successfully!",['connectiondetails' => $this->getAllConnectionDetails()]);
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    public function updateEncryptionParameters(Request $request)
    {
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$validator = Validator::make($request->all(), [
                'mode' => 'required',
                'key_size' => 'required',
                'init_vector' => 'required',
                'secret_key' => 'required'
	        ]);
	        if ($validator->fails()) {
	            return $this->resp(0,"Fill the required fields!",['error' => 'Fill the required fields!']);
	        }
    		
    		$this->setEncryptionParams("mode",$request->mode);
    		$this->setEncryptionParams("key_size",$request->key_size);
    		$this->setEncryptionParams("init_vector",$request->init_vector);
    		$this->setEncryptionParams("secret_key",$request->secret_key);

	        return $this->resp(1,"Encryption Params Updated Successfully!",['encryption_params' => $this->getAllEncryptParams()]);
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    public function getEncrypParamsForMobile(Request $request){
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	return $this->resp(1,"Encryption Parameters",['encrypt_params' => $this->getAllEncryptParams()]);
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    public function getConnectionDetailForMobile(Request $request){
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	return $this->resp(1,"Connection Details",['connection_details' => $this->getAllConnectionDetails()]);
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }
    public function getAllEmailTemplate(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
            $getVal = EmailContent::get();
            if($getVal){
                return $this->resp(1,"All Email Templates",['email_templates' => $getVal]);
            }
            else{
                return $this->resp(0,"Email Template Not Exists",['email_templates' => NULL]);
            }
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    function getEmailContent($val){
    	$getVal = EmailContent::select("*")->where('template_name',$val)->first();
        if($getVal){
            return $getVal;
        }
        else{
            return NULL;
        }
    }

    public function updateEmailTemp(Request $request){
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$validator = Validator::make($request->all(), [
                'template_name' => 'required',
                'subject' => 'required',
                'content' => 'required'
	        ]);
	        if ($validator->fails()) {
	            return $this->resp(0,"Fill the required fields!",['error' => 'Fill the required fields!']);
	        }

	        $this->setEmailTempUpdate($request->template_name,$request->subject,$request->content);

            return $this->resp(1,"Email Template Updated",['email_template' => $this->getEmailContent($request->template_name)]);
	    }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    function setEmailTempUpdate($val,$newSubject,$newContent){
    	$getVal = EmailContent::select("*")->where('template_name',$val)->first();
    	if($getVal){
    		$getVal->subject = $newSubject;
    		$getVal->content = $newContent;
    		$getVal->save();
    	}
    	else{
    		$updatCon = new EmailContent();
    		$updateCon->subject = $newSubject;
    		$updateCon->content = $newContent;
    		$updateCon->save();
    	}
    	return;
    }

    function getAllConnectionDetails(){
    	$getVal = ConnectionDetails::select("*")->get();
    	if($getVal){
    		return $getVal;
    	}
    	else{
    		return NULL;
    	}
    }

    function getConnectionDetails($val){
    	$getVal = ConnectionDetails::select("*")->where('attribute_name',$val)->first();
    	if($getVal){
    		return $getVal->attrib_value;
    	}
    	else{
    		return NULL;
    	}
    }

    function getAllEncryptParams(){
    	$getVal = EncryptionParams::select("*")->get();
    	if($getVal){
    		return $getVal;
    	}
    	else{
    		return NULL;
    	}
    }

    function getEncryptionParams($val){
    	$getVal = EncryptionParams::select("*")->where('attribute_name',$val)->first();
    	if($getVal){
    		return $getVal->attrib_value;
    	}
    	else{
    		return NULL;
    	}
    }

	function setEncryptionParams($val,$newValue){
    	$getVal = EncryptionParams::select("*")->where("attribute_name",$val)->first();
    	if($getVal){
    		$getVal->attrib_value = $newValue;
    		$getVal->save();
    	}
    	else{
    		$updatCon = new EncryptionParams();
    		$updateCon->attribute_name = $val;
    		$updateCon->attrib_value = $newValue;
    		$updateCon->save();
    	}
    	return;
    }

    function setConnectionDetails($val,$newValue){
    	$getVal = ConnectionDetails::select("*")->where("attribute_name",$val)->first();
    	if($getVal){
    		$getVal->attrib_value = $newValue;
    		$getVal->save();
    	}
    	else{
    		$updatCon = new ConnectionDetails();
    		$updateCon->attribute_name = $val;
    		$updateCon->attrib_value = $newValue;
    		$updateCon->save();
    	}
    	return;
    }

	function getUserEmail($firstRecord){
		$userData = Users::find($firstRecord['user_id']);
		if($userData){
	    	$final = array('email'=>$userData->email,'first_name'=>$userData->first_name,'last_name'=>$userData->last_name);
	    	return $final;
		}
		else{
			return $this->resp(0,'User Record Not Found',NULL);
			return NULL;
		}
	}

	function create_csv_string($data,$filename) {
	  // Open temp file pointer
	  file_put_contents($filename,'content');
	//   dd($filename);
	  if (!$fp = fopen($filename, 'w+')) return FALSE;

	  $header = ["Description","Location","Serial Number","Stamp Status","Tax Class","Intended Market","Stamp","Assessment Number","Manufacturer","Brand","Place of Production","ID","Scan ID","Scan Date","Scanner ID","User","Valid Code","GPS Location","Display Name","Create Date"];
	  
	  fputcsv($fp, $header);
	  // Loop data and write to file pointer
	  foreach ($data as $line) {
	  	$newLine = array(
	      "description"=>$line['desc'],
	      "location"=>$line['location'],
	      "serial_number"=>$line['scanner_sr_no'],
	      "stamp_status"=>$line['stamp_status'],
	      "tax_class"=>$line['tax_class'],
	      "intended"=>$line['intended'],
	      "stamp"=>$line['stamp_date'],
	      "assessment_number"=>$line['assessment_number'],
	      "manufacturer"=>$line['manufacturer'],
	      "brand"=>$line['brand'],
	      "place_of_production"=>$line['place_of_production'],
	      "id"=>$line['id'],
	      "scan_id"=>$line['name'],
	      "scan_date"=>$line['scan_date'],
	      "scanner_id"=>$line['scanner_id'],
	      "user_id"=>$line['user_id'],
	      "valid_code"=>$line['valid_code'],
	      "gps_location"=>$line['gps_location'],
	      "display_name"=>$line['display_name'],
	      "create_date"=>$line['create_date'],
	    );

	  	fputcsv($fp, $newLine);
	  }
	  // Place stream pointer at beginning
	  rewind($fp);

	  // Return the data
	  return stream_get_contents($fp);
	}

    function checkToken($token){
        $checkt = UserSession::where('remember_token','=',$token)->first();
        if($checkt){
            return 1;
        }
        else {
            return 0;
        }
    }

    function resp($success, $message, $data = [])
    {
        $resp ['success'] = $success;
        $resp['message'] = $message;
        if (!empty($data)){
            $resp['data'] = $data;
        }
        return response()->json($resp);
    }
}
