<?php

namespace App\Http\Controllers\Api;
use App\MyLib\ripcord;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Validator;
use App\Models\VehicleStatus;
use App\Models\UserSession;
use App\Models\UserLogs;
use App\Models\Users;
use App\Models\TokenCode;
use App\Models\EmailContent;
use App\Models\ConnectionDetails;
use App\Models\EncryptionParams;
use Mail;

class MobileController extends Controller
{
    
	public function mobileSignUp(Request $request){
		$validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'confirm_password' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->resp(0,"Registration Failed",['error' => 'Fill the required fields!']);
        }
        if ($request->password != $request->confirm_password){
            return $this->resp(0,"Registration Failed",['error' => 'Password Do Not Match!']);
        }

    	$data = $request->all();
    	$checkpass = crypt($request->password, md5($request->email));
    	$dup = Users::select('*')->where('email','=',$request->email)->first();
    	if ($dup){
            return $this->resp(0,"Registration Failed",['error' => 'Email Already Exist!']);    		
    	}
    	$newUser = new Users();
    	$newUser->first_name = $request->first_name;
    	$newUser->last_name = $request->last_name;
    	$newUser->email = $request->email;
        $newUser->password = $checkpass;
        $newUser->role_id = 2;
    	$newUser->status = 0;
    	$newUser->save();


    	$six_digit_random_number = mt_rand(100000, 999999);
    	$token = new TokenCode();
    	$token->tokenforemail = $six_digit_random_number;
    	$token->user_id = $newUser->id;
    	$token->save();

		//$newUser->tokenotp = $six_digit_random_number;
		//$newUser->note = "This above code is only for testing purposes via API will be removed after that.";


        $emailTemplate = $this->getEmailContent("sign_up");
        // dump($emailTemplate);
        $msg = $emailTemplate->content.$six_digit_random_number;
    	// the message
		//$msg = "Email Verification Code\nKindly Enter the Below Email Verification Code to verify your email\n".$six_digit_random_number;

		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email

        $email = $request->email;
        $data = ['messages' => $msg];
        try{
            $checkError = Mail::send('mail', $data, function($message) use($email) {
                $message->to($email, 'Sign Up')->subject
                ('Verification Code');
                $message->from('emailer@irsis.ph','Trackrmobile');
            });
            //$checkError = mail($request->email,"Email Verification",$msg);
            if(Mail::failures()){
                $newUser->emailstatus = "Email Not Sent";
                $newUser->error = "mail failure";
            }
            else{
                $newUser->emailstatus = "Email Sent";
            }
        }
        catch(\Exception $e){
            $newUser->emailstatus = "Email Not Sent";
            $newUser->error = "".$e->getMessage();
        }

    	return $this->resp(1,"Check your email for verifcation code",['user' => $newUser]);
	}

	public function checktable(Request $request){
		$check = $request->table;
		$table = DB::Table($check)->select('*')->get();

    	return $this->resp(1,"Check your email for verifcation code",['user' => $table]);
	}


	public function otpConfirmViaEmail(Request $request){
		$tokenValue = $request->confirmtoken;
		$userid = $request->userid;
		$tokenmatch = TokenCode::select('*')->where('user_id','=',$userid)->where('tokenforemail','=',$tokenValue)->first();
		if($tokenmatch){
			$useractive = Users::find($userid);
			$useractive->status = 1;
			$useractive->save();

			$delTokenCode = TokenCode::find($tokenmatch)->first()->delete();

    		return $this->resp(1,"Signup Successful",['user' => $useractive]);
		}
		else {
    		return $this->resp(0,"Invalid Pin",['user' => NULL]);
		}
	}

    public function mobilelogin(Request $request){
    	if($request->IsMethod("post")){
	    	$checkpass = crypt($request->password, md5($request->email));

	        $userCheck = Users::select('*')->where('email',$request->email)->where('password',$checkpass)->first();
	        
	        if ($userCheck){
	        	if(!$userCheck->status){
	        		return $this->resp(0,"Login Failed",['error' => 'Account not active!']);
	        	}
	            $str=rand(); 
	            $result = md5($str);
	            $logintoken = $result;

	            $mobileloginuser = new UserSession();
	            $mobileloginuser->user_id = $userCheck->id;
	            $mobileloginuser->remember_token = $logintoken;
	            $mobileloginuser->save();

	            $userCheck->token = $logintoken;

	            $token = $logintoken;

	            return $this->resp(1,"Login Successful",['user' => $userCheck]);
	        }
	        else {
	        	return $this->resp(0,"Login Failed",['error' => 'Wrong email or password!']);
	        }
    	}
    }
    
    public function forgetPassword(Request $request){
    	if($request->IsMethod("post")){
    		$email = $request->email;
	    	$newUser = Users::select('*')->where('email','=',$email)->first();
	    	if($newUser){
	    		$six_digit_random_number = mt_rand(100000, 999999);
		    	$token = new TokenCode();
		    	$token->tokenforemail = $six_digit_random_number;
		    	$token->user_id = $newUser->id;
		    	$token->save();
				//$newUser->tokenotp = $six_digit_random_number;

                $emailTemplate = $this->getEmailContent("forgot_password");

                $msg = $emailTemplate->content.$six_digit_random_number;

		    	// the message
				//$msg = "Forget Password\nKindly Enter the Below Verification Code to verify it's You\n".$six_digit_random_number;

				// use wordwrap() if lines are longer than 70 characters
				$msg = wordwrap($msg,70);

                $data = ['messages' => $msg];
                $email = $request->email;
				// send email
				//$checkError = mail($request->email,"Forgot Password",$msg);
                $checkError = Mail::send('mail', $data, function($message) use($email) {
                     $message->to($email, 'Trackrmobile')->subject('Forgot Password');
                     $message->from('emailer@irsis.ph','Trackrmobile');
                });
                if (Mail::failures()){
                    $newUser->emailstatus = "Email Not Sent";
                    return $this->resp(0,"Unable to Send Code",['user' => $newUser]);

                }
				else{
					$newUser->emailstatus = "Email Sent";
		        	return $this->resp(1,"Code Sent Successfully",['user' => $newUser]);
				}
				/*else{
					$newUser->emailstatus = "Email Not Sent";
		        	return $this->resp(0,"Unable to Send Code",['user' => $newUser]);
				}*/
	    	}
	    	else{
	    		return $this->resp(0,"User Does Not Exist",['user' => NULL]);
	    	}    		
    	}
    }


    public function matchforgotpasswordCode(Request $request){
    	$tokenValue = $request->confirmtoken;
		$userid = $request->userid;
		$tokenmatch = TokenCode::select('*')->where('user_id','=',$userid)->where('tokenforemail','=',$tokenValue)->first();
		$userCheck = Users::select('*')->where('id','=',$userid)->first();
		if($tokenmatch){
			if(!$userCheck){
    			return $this->resp(0,"User Does Not Exist",['user' => NULL]);
			}
	       	$str=rand(); 
	        $result = md5($str);
	        $logintoken = $result;

	        $mobileloginuser = new UserSession();
	        $mobileloginuser->user_id = $userCheck->id;
	        $mobileloginuser->remember_token = $logintoken;
	        $mobileloginuser->save();

	        $userCheck->token = $logintoken;

	       	$token = $logintoken;

	       	$userCheck->token = $token;

			$delTokenCode = TokenCode::find($tokenmatch)->first()->delete();

    		return $this->resp(1,"Email Verified",['user' => $userCheck]);
		}
		else {
    		return $this->resp(0,"Invalid Pin",['user' => NULL]);
		}
    }

    public function newPassword(Request $request){
    	$data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$validator = Validator::make($request->all(), [
                'password' => 'required',
                'confirm_password' => 'required'
	        ]);
	        if ($validator->fails()) {
	            return $this->resp(0,"Change Password Failed",['error' => 'Fill the required fields!']);
	        }
	        if ($request->password != $request->confirm_password){
	            return $this->resp(0,"Change Password Failed",['error' => 'Password Do Not Match!']);
	        }

	    	$newUser = Users::find($request->id);
	    	if(!$newUser){
            	return $this->resp(0,"User Does not exist",['user' => NULL]);
	    	}
	    	$checkpass = crypt($request->password, md5($newUser->email));
	    	//$newUser = Users::select('*')->where('id','=',$request->id)->first();
	    	$newUser->password = $checkpass;
	    	$newUser->save();
            return $this->resp(1,"Password Change Successful",['user' => $newUser]);
	    }
	    else {
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    public function makeAdminUserAccount(Request $request){
        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
            $validator = Validator::make($request->all(), [
                'user_id' => 'required'
            ]);
            if ($validator->fails()) {
                return $this->resp(0,"Fill the required fields!",['error' => 'Fill the required fields!']);
            }

            $checkt = UserSession::where('remember_token','=',$data['token'])->first();
            if($checkt){
                // $newAdmin = Users::find($request->user_id);
                // if($newAdmin){
                //     $newAdmin->role_id = $request->role_id;
                //     $newAdmin->save();
                //     return $this->resp(1,"User role updated!",['user' => $newAdmin]);
                // }
                $admi = Users::find($checkt->user_id);
                if($admi && $admi->role_id == 1){
                    $newAdmin = Users::find($request->user_id);
                    if($newAdmin){
                        $newAdmin->role_id = $request->role_id;
                        $newAdmin->save();
                        return $this->resp(1,"User role updated!",['user' => $newAdmin]);
                    }
                    return $this->resp(0,"User Does not exist!",['user' => NULL]);
                }
                return $this->resp(0,"Only Admin can change role",['user' => NULL]);
            }
            
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        else {
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    function checkToken($token){
        $checkt = UserSession::where('remember_token','=',$token)->first();
        if($checkt){
            return 1;
        }
        else {
            return 0;
        }
    }

    public function mobileUsers(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$users = Users::select('*')->get();

            return $this->resp(1,"Users Record",['user' => $users]);
        }
        else {
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    public function userAccountActive(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$userid = $request->id;
        	$userwId = Users::select('*')->where('id','=',$userid)->first();
        	if($userwId){
        		$userwId->status = 1;
        		$userwId->save();
            
            	return $this->resp(1,"Account Activated",['user' => $userwId]);
        	}
        	else {
            	return $this->resp(0,"Account Not Found",['user' => NULL]);
        	}
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    public function userAccountUnActive(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
        	$userid = $request->id;
        	$userwId = Users::select('*')->where('id','=',$userid)->first();
        	if($userwId){
        		$userwId->status = 0;
        		$userwId->save();
            
            	return $this->resp(1,"Account Deactivated",['user' => $userwId]);
        	}
        	else {
            	return $this->resp(0,"Account Not Found",['user' => NULL]);
        	}
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }


    public function userAccountDelete(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){
            $userid = $request->id;
            $userwId = Users::select('*')->where('id','=',$userid)->first();
            if($userwId){
                $userwId->delete();
            
                return $this->resp(1,"Account Deleted",['user' => NULL]);
            }
            else {
                return $this->resp(0,"Account Not Found",['user' => NULL]);
            }
        }
        else{
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    function getEmailContent($val){
        $getVal = EmailContent::select("*")->where('template_name',$val)->first();
        if($getVal){
            return $getVal;
        }
        else{
            return NULL;
        }
    }

    public function mobilelogout(Request $request){

        $data = $request->all();
        if(!isset($data['token'])){
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
        if($this->checkToken($data['token']) == 1){

            $userentry = UserSession::where('remember_token','=',$data['token'])->delete();

            return $this->resp(1,"Logout Successfull",['user' => NULL]);
        }
        else {
            return $this->resp(0,"Token Mismatch",['user' => NULL]);
        }
    }

    function resp($success, $message, $data = [])
    {
        $resp ['success'] = $success;
        $resp['message'] = $message;
        if (!empty($data)){
            $resp['data'] = $data;
        }
        return response()->json($resp);
    }


    public function odooshitcalling(Request $request){
        $url = "https://trackrssl.topc.ph";
        $db = "custome";
        $username = "dev";
        $password = "dev";

        //require_once('ripcord.php');
        /*$info = ripcord::client('https:trackrssl.topc.ph')->start();
        list($url, $db, $username, $password) = array($info['host'], $info['database'], $info['user'], $info['password']);*/

        $common = ripcord::client("$url/xmlrpc/2/common");
        $common->version();

        $uid = $common->authenticate($db, $username, $password, array());

        $models = ripcord::client("$url/xmlrpc/2/object");
        /*$models->execute_kw($db, $uid, $password,
            'res.partner', 'check_access_rights',
            array('read'), array('raise_exception' => false));


        */
        $models->execute_kw($db, $uid, $password,
            'stock.quant', 'search_read',
            array(array(array('lot_id', '=', true))));

    }
}
