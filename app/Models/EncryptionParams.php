<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EncryptionParams extends Model
{
    protected $table = "encryption_parameter";
}
