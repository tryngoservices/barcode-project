<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnectionDetails extends Model
{
    protected $table = 'connection_details';
}
