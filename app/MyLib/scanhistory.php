<?php 
$url = "https://trackrgamma.topc.ph";
$db = "trackrgamma";
/*$url = "https://trackrssl.topc.ph";
$db = "custom";*/
$username = "dev";
$password = "dev";

require_once('ripcord.php');
/*$info = ripcord::client('https:trackrssl.topc.ph')->start();
list($url, $db, $username, $password) = array($info['host'], $info['database'], $info['user'], $info['password']);*/

$scanid = (isset($_POST['scanid'])) ? $_POST['scanid'] : NULL;
$scannerid = (isset($_POST['scannerid'])) ? $_POST['scannerid'] : NULL;
$user = (isset($_POST['user'])) ? $_POST['user'] : NULL;
$datetime = (isset($_POST['datetime'])) ? $_POST['datetime'] : NULL;
$gps = (isset($_POST['gps'])) ? $_POST['gps'] : NULL;
$uic = (isset($_POST['uic'])) ? $_POST['uic'] : NULL;
$manufacturer = (isset($_POST['manufacturer'])) ? $_POST['manufacturer'] : NULL;
$brand = (isset($_POST['brand'])) ? $_POST['brand'] : NULL;
$productionplace = (isset($_POST['productionplace'])) ? $_POST['productionplace'] : NULL;
$tax = (isset($_POST['tax'])) ? $_POST['tax'] : NULL;
$intendedmarket = (isset($_POST['intendedmarket'])) ? $_POST['intendedmarket'] : NULL;
$stamp = $_POST['stamp'];
$assessmentnumber = $_POST['assessmentnumber'];
$stampstatus = $_POST['stampstatus'];
$validcode = $_POST['validcode'];
$description = $_POST['description'];
$location = $_POST['location'];

//$scanid == NULL || 

if($scannerid == NULL || $user == NULL || $datetime == NULL || $gps == NULL || $uic == NULL || $manufacturer == NULL || $brand == NULL || $productionplace == NULL || $tax == NULL || $intendedmarket == NULL || $stamp == NULL || $assessmentnumber == NULL || $stampstatus == NULL || $validcode == NULL){
	echo "Required Fields Empty";
	return;
}
/*
//oScan ID – unique identifier of each scan, name
//oScanner ID – unique identifier of the scanner used
//oUser logged in the mobile device
//oDatetime of the scan
//oGPS of the location where the scan was performed, gps_location
//oUnique Identification Code (UIC)
//oManufacturer
//oBrand
//oPlace of Production, place_of_production
oTax Class
oIntended Market
oStamp Order Date
oAssessment Number
oStamp Status
oValid Code*/
//"name"=>$scanid,
$uniqScanId = $scannerid.date('Ymdhisu');
$alldate = array(
	"id"=>$scanid,
	"name"=>$scanid,
	"scanner_id"=>$scannerid,
	"user_id"=>$user,
	"scan_date"=>$datetime,
	"gps_location"=>$gps,
	"scanner_sr_no"=>$uic,
	"manufacturer"=>$manufacturer,
	"brand"=>$brand,
	"place_of_production"=>$productionplace,
	"tax_class"=>$tax,
	"intended"=>$intendedmarket,
	"stamp_date"=>$stamp,
	"assessment_number"=>$assessmentnumber,
	"stamp_status"=>$stampstatus,
	"valid_code"=>$validcode,
	"location"=>$location,
	"desc"=>$description
);

/*$allArrUpdate = array(
    'name'=>$scanid,
    'desc'=>$description,
    'location'=>$location,
    'scanner_id'=>$scannerid,
    'user_id'=>$user,
    'brand'=>$brand,
    'tax_class'=>$tax,
    'stamp_date'=>$datetime,
    'scan_date'=>$datetime,
    'manufacturer'=>$manufacturer,
    'place_of_production'=>$productionplace,
    'intended'=>$intendedmarket,
    'assessment_number'=>$assessmentnumber,
    'valid_code'=>$validcode,
    'scanner_sr_no'=>$scannerid
);*/

$common = ripcord::client("$url/xmlrpc/2/common");
$version = $common->version();

//echo "Version: ".print_r($version);
//print_r($common);

$uid = $common->authenticate($db, $username, $password, array());
//echo "This is uid: ".$uid;
$models = ripcord::client("$url/xmlrpc/2/object");
/*$checkAccess = $models->execute_kw($db, $uid, $password,
    'stock.scan.history', 'check_access_rights',
    array('read'), array('raise_exception' => false));*/
//echo "Access Under It:<br>";
//print_r($checkAccess);


$values = $models->execute_kw($db, $uid, $password, 'stock.scan.history', 'create', array($alldate));
header("Content-Type: application/json;charset=utf-8");

if(empty($values) || $values['faultCode']){
	$resp['success'] = 0;
	$resp['message'] = "Record Creation Failed";
	if (!empty($values)){
	    $resp['data'] = null;
	}

	$jsonEnc = json_encode($resp);
	/*//print_r($jsonEnc);*/
	print_r($jsonEnc);
	return;
}

$resp['success'] = 1;
$resp['message'] = "Record Created";
if (!empty($values)){
    $resp['data'] = $values;
    $resp['scan_id'] = $scanid;
}

$jsonEnc = json_encode($resp);
/*//print_r($jsonEnc);*/
print_r($jsonEnc);

?>