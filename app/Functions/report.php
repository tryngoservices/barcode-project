<?php 
require_once('ripcord.php');

function makeReportofIds($ids,$url,$db,$username,$password){
  $email = NULL;
  // $ids = $_POST['ids'];
  $idarray = array();
  foreach($ids as $singleId){
  	$idarray[] = intval($singleId);
  }

  $common = ripcord::client("$url/xmlrpc/2/common");
  $version = $common->version();

  /*echo "Version: ".$version;
  print_r($common);*/
  $uid = $common->authenticate($db, $username, $password, array());
  //echo "This is uid: ".$uid;
  $models = ripcord::client("$url/xmlrpc/2/object");

  $values = $models->execute_kw($db, $uid, $password, 'stock.scan.history', 'search_read', array(array(array('id','=',$idarray))));


  if(empty($values)){
  	return NULL;
  }
  else {
    return $values;
  }
}


function send_mail($email,$from_email,$subject,$message){

	$filename = "temp.csv";
	$size = filesize($filename);
	$handle = fopen($filename, "r");  // set the file handle only for reading the file 
    $content = fread($handle, $size); // reading the file 
    fclose($handle);                  // close upon completion 
  
    $encoded_content = chunk_split(base64_encode($content));
  
    $boundary = md5("random"); // define boundary with a md5 hashed value 
  
    //header 
    $headers = "MIME-Version: 1.0\r\n"; // Defining the MIME version 
    $headers .= "From:".$from_email."\r\n"; // Sender Email 
    $headers .= "Reply-To:".$from_email."\r\n"; // Email addrress to reach back 
    $headers .= "Content-Type: multipart/mixed;\r\n"; // Defining Content-Type 
    $headers .= "boundary = $boundary\r\n"; //Defining the Boundary 
          
    //plain text  
    $body = "--$boundary\r\n"; 
    $body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n"; 
    $body .= "Content-Transfer-Encoding: base64\r\n\r\n";  
    $body .= chunk_split(base64_encode($message));  
          
    //attachment 
    $body .= "--$boundary\r\n"; 
    $body .="Content-Type: text/csv; name=temp.csv\r\n"; 
    $body .="Content-Disposition: attachment; filename=temp.csv\r\n"; 
    $body .="Content-Transfer-Encoding: base64\r\n"; 
    $body .="X-Attachment-Id: ".rand(1000, 99999)."\r\n\r\n";  
    $body .= $encoded_content; // Attaching the encoded file with email 
      
    $sentMailResult = mail($email, $subject, $body, $headers); 
  
    if($sentMailResult)  
    { 
       echo "File Sent Successfully."; 
       sendResponse(1,"Email Sent",NULL);
       // unlink($name); // delete the file after attachment sent. 
    } 
    else
    { 
       echo "Sorry but the email could not be sent. 
                    Please go back and try again!"; 
       sendResponse(0,"Email Sent Failed",NULL);
    }
}

//$array = array(array(1,2,3,4,5,6,7), array(1,2,3,4,5,6,7), array(1,2,3,4,5,6,7));

?>