<?php

require_once('ripcord.php');

function clientPHP($value,$url,$db,$username,$password){
	if(!isset($value)){
		return NULL;
	}

	$searchvalue = $value;

	$common = ripcord::client("$url/xmlrpc/2/common");
	//$version = $common->version();
	//echo "Version: ".$common;
	$uid = $common->authenticate($db, $username, $password, array());

	$models = ripcord::client("$url/xmlrpc/2/object");
	/*$models->execute_kw($db, $uid, $password,
	    'res.partner', 'check_access_rights',
	    array('read'), array('raise_exception' => false));
	*/
	$values1 = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'search_read', [[['uic','=',$searchvalue]]]);
	if(empty($values1) || $values1['faultCode']){
		return NULL;
	}
	$lotids = array();
	foreach($values1 as $lot){
		$lotids[] = intval($lot['lot_id']);
	}

	$values2 = $models->execute_kw($db, $uid, $password, 'stock.quant', 'search_read', [[['lot_id', '=', $searchvalue]]]);

	if(empty($values2) || $values2['faultCode']){
		return NULL;
	}
	$locationids = array();
	$manufacturer = array();
	$brand = array();
	$productionplace = array();
	foreach($values2 as $location){
		$locationids[] = intval($location['location_id']);
		$manufacturer[] = $location['manufacturer'];
		$brand[] = $location['brand'];
		$productionplace[] = $location['place_of_production'];
	}

	$values3 = $models->execute_kw($db, $uid, $password, 'stock.location', 'search_read', [[['id', '=', $locationids]]]);

	if(empty($values3) || $values3['faultCode']){
		return NULL;
	}
	//,array('fields'=>array('usage'))
	$values1 = $models->execute_kw($db, $uid, $password, 'stock.quant', 'search_read', [[['lot_id', '=', $searchvalue]]]);

	if(empty($values1)){
		return NULL;	
	}

	$values2 = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'search_read', [[['name', '=', $searchvalue]]]);

	if(empty($values2)){
		return NULL;
	}

	$merged = array_merge($values1,$values2,$values3);

	return $merged;
}

function clientPHP1($value,$url,$db,$username,$password){

$http_response_header = "{'_response':'\n\n\n\n\n\nserver_version<\/name>\n13.0<\/string><\/value>\n<\/member>\n\nserver_version_info<\/name>\n\n13<\/int><\/value>\n0<\/int><\/value>\n0<\/int><\/value>\nfinal<\/string><\/value>\n0<\/int><\/value>\n<\/string><\/value>\n<\/data><\/array><\/value>\n<\/member>\n\nserver_serie<\/name>\n13.0<\/string><\/value>\n<\/member>\n\nprotocol_version<\/name>\n1<\/int><\/value>\n<\/member>\n<\/struct><\/value>\n<\/param>\n<\/params>\n<\/methodResponse>\n','_request':'\n\nversion<\/methodName>\n\n<\/methodCall>\n','_throwExceptions':false,'_autoDecode':true}";
//require_once('ripcord.php');
/*$info = ripcord::client('https:trackrssl.topc.ph')->start();
list($url, $db, $username, $password) = array($info['host'], $info['database'], $info['user'], $info['password']);*/
if(!isset($_GET['value'])){
	$resp['success'] = 0;
	$resp['message'] = "Field Empty.";
	if (!empty($_GET['value'])){
	    $resp['data'] = NULL;
	}

	$jsonEnc = json_encode($resp);
	/*//print_r($jsonEnc);*/
	print_r($jsonEnc);
	return;
}
$searchvalue = $_GET['value'];

$common = ripcord::client("$url/xmlrpc/2/common");
$version = $common->version();
/*echo "Version: ".$version;*/
print_r(json_encode($common));
$uid = $common->authenticate($db, $username, $password, array());

$models = ripcord::client("$url/xmlrpc/2/object");
/*$models->execute_kw($db, $uid, $password,
    'res.partner', 'check_access_rights',
    array('read'), array('raise_exception' => false));


*/
$values1 = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'search_read', [[['uic','=',$searchvalue]]]);
$lotids = array();
foreach($values1 as $lot){
	$lotids[] = intval($lot['lot_id']);
}

$values2 = $models->execute_kw($db, $uid, $password, 'stock.quant', 'search_read', [[['lot_id', '=', $searchvalue]]]);
$locationids = array();
$manufacturer = array();
$brand = array();
$productionplace = array();
foreach($values2 as $location){
	$locationids[] = intval($location['location_id']);
	$manufacturer[] = $location['manufacturer'];
	$brand[] = $location['brand'];
	$productionplace[] = $location['place_of_production'];
}

$values3 = $models->execute_kw($db, $uid, $password, 'stock.location', 'search_read', [[['id', '=', $locationids]]]);

/*$merged = array_merge($values3,$manufacturer,$brand,$productionplace);
$jsonEnc = json_encode($merged);
print_r($jsonEnc);*/

//,array('fields'=>array('usage'))
$values1 = $models->execute_kw($db, $uid, $password, 'stock.quant', 'search_read', [[['lot_id', '=', $searchvalue]]]);

if(empty($values1)){
	$resp['success'] = 0;
	$resp['message'] = "Record Empty.";
	if (!empty($values)){
	    $resp['data'] = NULL;
	}

	$jsonEnc = json_encode($resp);
	/*//print_r($jsonEnc);*/
	print_r($jsonEnc);
	return;	
}

$values2 = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'search_read', [[['name', '=', $searchvalue]]]);

if(empty($values2)){
	$resp['success'] = 0;
	$resp['message'] = "Record Empty.";
	if (!empty($values)){
	    $resp['data'] = NULL;
	}

	$jsonEnc = json_encode($resp);
	/*//print_r($jsonEnc);*/
	print_r($jsonEnc);
	return;
}

$merged = array_merge($values1,$values2,$values3);
$resp['success'] = 1;
$resp['message'] = "Record Get";
if (!empty($merged)){
	$resp['data'] = $merged;
}

$jsonEnc = json_encode($resp);
print_r($jsonEnc);
return;
}
?>