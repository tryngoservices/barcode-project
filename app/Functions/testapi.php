<?php 
require_once('ripcord.php');

function fetchRecord($value,$url,$db,$username,$password){
	if(!$value){
		return NULL;
	}
	$common = ripcord::client("$url/xmlrpc/2/common");
	$version = $common->version();
	/*echo "Version: ".$version;
	print_r($common);*/
	$uid = $common->authenticate($db, $username, $password, array());
	//echo "UID is: ".$uid;
	$models = ripcord::client("$url/xmlrpc/2/object");

	$sample_serial_no = $value;
	//,array('serial_no': $sample_serial_no)

	$result = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'get_serial_state',[[[]]],['serial_no'=> $sample_serial_no]);

	return $result;
}
?>