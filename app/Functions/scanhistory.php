<?php 
require_once('ripcord.php');

function insertScanHistory($data,$url,$db,$username,$password){

	$scanid = (isset($data['scanid'])) ? $data['scanid'] : '';
	$scannerid = (isset($data['scannerid'])) ? $data['scannerid'] : '';
	$user = (isset($data['user'])) ? $data['user'] : '';
	$datetime = (isset($data['datetime'])) ? $data['datetime'] : '';
	$gps = (isset($data['gps'])) ? $data['gps'] : '';
	$uic = (isset($data['uic'])) ? $data['uic'] : '';
	$manufacturer = (isset($data['manufacturer'])) ? $data['manufacturer'] : '';
	$brand = (isset($data['brand'])) ? $data['brand'] : '';
	$productionplace = (isset($data['productionplace'])) ? $data['productionplace'] : '';
	$tax = (isset($data['tax'])) ? $data['tax'] : '';
	$intendedmarket = (isset($data['intendedmarket'])) ? $data['intendedmarket'] : '';
	$stamp = (isset($data['stamp'])) ? $data['stamp'] : '';
	$assessmentnumber = (isset($data['assessmentnumber'])) ? $data['assessmentnumber'] : '';
	$stampstatus = (isset($data['stampstatus'])) ? $data['stampstatus'] : '';
	$validcode = (isset($data['validcode'])) ? $data['validcode'] : '';
	$description = (isset($data['description'])) ? $data['description'] : '';
	$location = (isset($data['location'])) ? $data['location'] : '';

	//$scanid == NULL || 

	if($scannerid == '' || $user == '' || $datetime == '' || $gps == '' || $uic == '' || $manufacturer == '' || $brand == NULL || $productionplace == '' || $tax == '' || $intendedmarket == '' || $stamp == '' || $assessmentnumber == ''  || $validcode == ''){
		return 'error';
	}
	/*
	//oScan ID – unique identifier of each scan, name //|| $stampstatus == NULL
	//oScanner ID – unique identifier of the scanner used
	//oUser logged in the mobile device
	//oDatetime of the scan
	//oGPS of the location where the scan was performed, gps_location
	//oUnique Identification Code (UIC)
	//oManufacturer
	//oBrand
	//oPlace of Production, place_of_production
	oTax Class
	oIntended Market
	oStamp Order Date
	oAssessment Number
	oStamp Status
	oValid Code*/
	//"name"=>$scanid,

	if($datetime == ''){
		$datetime = date('Y-m-d h:i:su');
	}
	if($stamp == ''){
		$stamp = date('Y-m-d h:i:su');
	}




	$uniqScanId = $scannerid.date('Ymdhisu');
	$alldate = array(
		"id"=>$scanid,
		"name"=>$scanid,
		"scanner_id"=>$scannerid,
		"user_id"=>$user,
		"scan_date"=>$datetime,
		"gps_location"=>$gps,
		"scanner_sr_no"=>$uic,
		"manufacturer"=>$manufacturer,
		"brand"=>$brand,
		"place_of_production"=>$productionplace,
		"tax_class"=>$tax,
		"intended"=>$intendedmarket,
		"stamp_date"=>$stamp,
		"assessment_number"=>$assessmentnumber,
		"stamp_status"=>(isset($stampstatus)) ? $stampstatus : '',
		"valid_code"=>$validcode,
		"location"=>$location,
		"desc"=>$description
	);

	/*$allArrUpdate = array(
	    'name'=>$scanid,
	    'desc'=>$description,
	    'location'=>$location,
	    'scanner_id'=>$scannerid,
	    'user_id'=>$user,
	    'brand'=>$brand,
	    'tax_class'=>$tax,
	    'stamp_date'=>$datetime,
	    'scan_date'=>$datetime,
	    'manufacturer'=>$manufacturer,
	    'place_of_production'=>$productionplace,
	    'intended'=>$intendedmarket,
	    'assessment_number'=>$assessmentnumber,
	    'valid_code'=>$validcode,
	    'scanner_sr_no'=>$scannerid
	);*/
	// echo "All Date Recieved: \n";
	// print_r(json_encode($alldate));

	$common = ripcord::client("$url/xmlrpc/2/common");
	$version = $common->version();

	// echo "Version: ".print_r($version);
	//print_r($common);

	$uid = $common->authenticate($db, $username, $password, array());
	// echo "\n This is uid: ".print_r($uid);
	$models = ripcord::client("$url/xmlrpc/2/object");
	/*$checkAccess = $models->execute_kw($db, $uid, $password,
	    'stock.scan.history', 'check_access_rights',
	    array('read'), array('raise_exception' => false));*/
	//echo "Access Under It:<br>";
	//print_r($checkAccess);


	$values = $models->execute_kw($db, $uid, $password, 'stock.scan.history', 'create', array($alldate));
	// header("Content-Type: application/json;charset=utf-8");

	// echo "Odoo Returned: \n";
	// print_r(json_encode($values));


	if(empty($values) || $values['faultCode']){
		// print_r($values['faultCode']);
		return NULL;
	}
	$merged = array('data'=>$values,'scan_id'=>$scanid);
	return $merged;
}
?>