-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: tmobile
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `connection_details`
--

DROP TABLE IF EXISTS `connection_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connection_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attrib_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connection_details`
--

LOCK TABLES `connection_details` WRITE;
/*!40000 ALTER TABLE `connection_details` DISABLE KEYS */;
INSERT INTO `connection_details` VALUES (1,'host_url','https://dev.topc.ph','2020-11-26 09:44:07','2020-11-09 12:44:34'),(2,'db','DEV','2020-11-26 09:44:07','2020-11-09 12:44:34'),(3,'username','dev','2020-11-26 09:44:07','2020-11-09 12:44:34'),(4,'password','dev','2020-11-26 09:44:07','2020-11-09 12:44:34'),(5,'csv_path_absolute','https://trackrmobile.topc.ph/','2020-11-09 12:44:34','2020-11-09 12:44:34');
/*!40000 ALTER TABLE `connection_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_content`
--

DROP TABLE IF EXISTS `email_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_content`
--

LOCK TABLES `email_content` WRITE;
/*!40000 ALTER TABLE `email_content` DISABLE KEYS */;
INSERT INTO `email_content` VALUES (1,'sign_up','TrackrMobile Report','You are getting this email because you requested for a report on your scanning activity.\nKindly click the link below:','2020-12-09 13:20:16','2020-11-09 12:44:34'),(2,'forgot_password','Forgot Password','Forget password\nKindly enter the below verification code to verify it\'s you','2020-12-07 00:57:31','2020-11-09 12:44:34'),(3,'send_report','TrackrMobile Report','You are getting this email because you requested for a report on your scanning activity.\nKindly click the link below:\n','2020-11-09 12:44:34','2020-11-09 12:44:34');
/*!40000 ALTER TABLE `email_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encryption_parameter`
--

DROP TABLE IF EXISTS `encryption_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encryption_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attrib_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encryption_parameter`
--

LOCK TABLES `encryption_parameter` WRITE;
/*!40000 ALTER TABLE `encryption_parameter` DISABLE KEYS */;
INSERT INTO `encryption_parameter` VALUES (1,'mode','CBC','2020-11-25 09:37:28','2020-11-09 12:44:36'),(2,'key_size','256','2020-11-09 12:44:36','2020-11-09 12:44:36'),(3,'init_vector','KadyaNuTet-ewaKa','2020-11-09 12:44:36','2020-11-09 12:44:36'),(4,'secret_key','s6v8y/B?E(H+MbQeThWmZq4t7w!z$C&F','2020-11-09 12:44:36','2020-11-09 12:44:36');
/*!40000 ALTER TABLE `encryption_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobileemailotp`
--

DROP TABLE IF EXISTS `mobileemailotp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobileemailotp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tokenforemail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobileemailotp`
--

LOCK TABLES `mobileemailotp` WRITE;
/*!40000 ALTER TABLE `mobileemailotp` DISABLE KEYS */;
INSERT INTO `mobileemailotp` VALUES (1,1,'125286',NULL,'2020-11-20','2020-11-20'),(2,2,'541965',NULL,'2020-11-20','2020-11-20'),(3,3,'569171',NULL,'2020-11-20','2020-11-20'),(4,4,'959012',NULL,'2020-11-24','2020-11-24'),(5,4,'389801',NULL,'2020-11-24','2020-11-24'),(9,7,'251915',NULL,'2020-11-24','2020-11-24'),(7,5,'119320',NULL,'2020-11-24','2020-11-24'),(18,14,'226816',NULL,'2020-12-07','2020-12-07'),(11,8,'908836',NULL,'2020-11-24','2020-11-24'),(13,10,'931233',NULL,'2020-11-24','2020-11-24'),(19,15,'101734',NULL,'2020-12-08','2020-12-08');
/*!40000 ALTER TABLE `mobileemailotp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobilesessions`
--

DROP TABLE IF EXISTS `mobilesessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobilesessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobilesessions`
--

LOCK TABLES `mobilesessions` WRITE;
/*!40000 ALTER TABLE `mobilesessions` DISABLE KEYS */;
INSERT INTO `mobilesessions` VALUES (1,3,'jkowieroi2324234324','2020-11-20 12:25:38','2020-11-20','2020-11-20'),(2,3,'c9c8c885b68e3a4a92a3512086f8e898',NULL,'2020-11-20','2020-11-20'),(3,8,'456cc0f3a0960e8ec46ca4fe544c8d65',NULL,'2020-11-24','2020-11-24'),(4,7,'978658753fb8b7d08053ba81d5b3f33e',NULL,'2020-11-24','2020-11-24'),(5,8,'6ed15a2fb51533a5f7ae89e554e37820',NULL,'2020-11-24','2020-11-24'),(6,8,'a768f637d19e5450177fc5e53a2839fa',NULL,'2020-11-24','2020-11-24'),(7,8,'c68ca810ef0e77e7e37af6b7110e94ef',NULL,'2020-11-24','2020-11-24'),(8,8,'a46f19bebcfba4ef6050a3624dce84c3',NULL,'2020-11-24','2020-11-24'),(9,8,'fa825435a3f679e68dc83b5162a9697f',NULL,'2020-11-24','2020-11-24'),(10,8,'7802d68413ade1baba49154e14ed7ef4',NULL,'2020-11-24','2020-11-24'),(11,8,'3ebf35e62f7960498862c20b49061b0d',NULL,'2020-11-24','2020-11-24'),(12,8,'91ce856c96b22885209552c54e39af47',NULL,'2020-11-24','2020-11-24'),(13,10,'56d0796e777792ae652c6692506dffcd',NULL,'2020-11-24','2020-11-24'),(14,8,'1e82a11f7b5cc37e7c9cdad8a2d85ea7',NULL,'2020-11-24','2020-11-24'),(15,10,'8ea0ccdf5cf0aa5f3174e4d23a6b9664',NULL,'2020-11-24','2020-11-24'),(16,10,'1e671a5bbf724f72314e2659eebb5ba0',NULL,'2020-11-24','2020-11-24'),(17,8,'3318a507471bbfdbc6a261276e69039f',NULL,'2020-11-24','2020-11-24'),(18,8,'95673fe3050d9972de3acd8154ede321',NULL,'2020-11-24','2020-11-24'),(19,10,'7225863127062f0bdd6b5eb67ee8bfa7',NULL,'2020-11-24','2020-11-24'),(20,8,'07515bf58a3b54cc29bba5f910729a3e',NULL,'2020-11-24','2020-11-24'),(21,8,'6c76e131fb8949bbcefebb427f007a72',NULL,'2020-11-25','2020-11-25'),(22,7,'c5d26202c1d0ddaa2f6adb7b3e63b3f3',NULL,'2020-11-25','2020-11-25'),(23,6,'893ac6838c1bf2b303a10d285650d26d',NULL,'2020-11-26','2020-11-26'),(24,8,'109b8cbf95b179747716f8cb0ba6b1cb',NULL,'2020-11-26','2020-11-26'),(25,8,'18a13b3cdc2941ca0cf35543834c9184',NULL,'2020-12-07','2020-12-07'),(26,6,'f8c787784156fd29dcf0f78a926c4b2f',NULL,'2020-12-07','2020-12-07'),(27,13,'ec563015dbbd91db26a9e79e73b60ec6',NULL,'2020-12-07','2020-12-07'),(28,14,'98e4297fcf8b422c0747258d3e97a3b4',NULL,'2020-12-07','2020-12-07'),(29,12,'3c7a52dd5aa4a17864f43918f960c4e1',NULL,'2020-12-08','2020-12-08'),(30,8,'24a8be5f61e07a69edd3f666ab3c41a2',NULL,'2020-12-08','2020-12-08'),(31,8,'1be048956fd7976d384e5ade31f2791a',NULL,'2020-12-08','2020-12-08'),(32,8,'e57712d97278d273b92404e991399a8e',NULL,'2020-12-08','2020-12-08'),(33,8,'fa8a73fbfd11809150f9c740c133aef3',NULL,'2020-12-08','2020-12-08'),(34,8,'7f00867b7b582df3734b9585ba4f3e7b',NULL,'2020-12-08','2020-12-08'),(35,8,'7a12a44a48b246e7e38c995620fa0aeb',NULL,'2020-12-08','2020-12-08'),(36,16,'fe3b9775ea71aa068a9c792cbded7ede',NULL,'2020-12-08','2020-12-08'),(37,16,'653aa80c376d4fb0b632ab87c374602f',NULL,'2020-12-08','2020-12-08'),(38,16,'26cc92d7a6667d120de59e76071b8b2b',NULL,'2020-12-08','2020-12-08'),(39,8,'dfc4a4cf95b9c55b36a4a824990c2b6b',NULL,'2020-12-08','2020-12-08'),(40,16,'e8b6eee8693440a1e0e74b4769050df9',NULL,'2020-12-08','2020-12-08'),(41,8,'2f185e3241812637378f903ca130b890',NULL,'2020-12-08','2020-12-08'),(42,16,'3b464a2f55d9361cd6ea132daf898de3',NULL,'2020-12-08','2020-12-08'),(43,15,'993296b5cfc8345e22584b0bcb734d70',NULL,'2020-12-08','2020-12-08'),(44,8,'e07c3bfa3096bf886e3a34922ea3c4bd',NULL,'2020-12-08','2020-12-08'),(45,15,'14d3641a4c651a43b816022bc915c086',NULL,'2020-12-08','2020-12-08'),(46,15,'4617ceb7a69f1b0d4794c59622610441',NULL,'2020-12-08','2020-12-08'),(47,8,'f53a91a7e34e2dfdc7fac27bf0570905',NULL,'2020-12-08','2020-12-08'),(48,6,'daf48227ad91c185d674eae5eb30751b',NULL,'2020-12-09','2020-12-09'),(49,6,'81742682003e1ca54e9b8bf37d20395f',NULL,'2020-12-09','2020-12-09'),(50,8,'ee41be42bd07ac8b6dc1a7e16989cb1f',NULL,'2020-12-09','2020-12-09'),(51,8,'51cb2ac7e33a12717ea6686270bf78a8',NULL,'2020-12-09','2020-12-09'),(52,8,'8fa08b4be5f1081ebf30a5a6fc9c9b2c',NULL,'2020-12-09','2020-12-09'),(53,6,'b175da2e5a02da315cfba6f45b06f302',NULL,'2020-12-09','2020-12-09'),(54,6,'0e895b835368f63ffe378baecc741ada',NULL,'2020-12-09','2020-12-09'),(55,8,'f0e250f4409bc3ee8ca0f26f0beb3c2d',NULL,'2020-12-09','2020-12-09'),(56,6,'b1f49f193689dd9bb6acf811356616b4',NULL,'2020-12-09','2020-12-09'),(57,17,'07dfc9e5b2f4acc8ef7097684efba775',NULL,'2020-12-09','2020-12-09'),(58,12,'097c054b987173635fde7e62ef09894b',NULL,'2020-12-09','2020-12-09'),(59,8,'953b8054fae5262de24c0b99c15ad3ed',NULL,'2020-12-09','2020-12-09'),(60,14,'a24f6a737a02e3efc1c5dd2689d998a6',NULL,'2020-12-09','2020-12-09'),(61,8,'991994b20ccf716d56060accefcd5e14',NULL,'2020-12-09','2020-12-09'),(62,8,'727a6e0a3f300e954057b919e21b01d0',NULL,'2020-12-09','2020-12-09'),(63,8,'df6fd92bbd804e6d6402ad60961cb3b7',NULL,'2020-12-09','2020-12-09'),(64,15,'c6bc15c8245a1a8b51af9b6e9cc5ad72',NULL,'2020-12-09','2020-12-09'),(65,8,'81b9e187f404e17e172b865784dc0293',NULL,'2020-12-09','2020-12-09'),(66,15,'0ca94d877532ed92676c4c48d647dc8f',NULL,'2020-12-09','2020-12-09'),(67,8,'a2b1bd7e10f862a25962ce0bf0a7f91f',NULL,'2020-12-09','2020-12-09'),(68,15,'c1357158bc194d8b129190842decef63',NULL,'2020-12-09','2020-12-09'),(69,15,'45c4989b7df55292618d31e6c04271a1',NULL,'2020-12-09','2020-12-09'),(70,6,'4f805149f6b2dbbe17f6e39d87534e52',NULL,'2020-12-10','2020-12-10'),(71,13,'2506679afcec1687661f204a530ba906',NULL,'2020-12-10','2020-12-10'),(72,8,'2420c07ae552951fefb4d5641e519f20',NULL,'2020-12-10','2020-12-10'),(73,8,'d37eaa71e47e1be352c28e2ffb2ee046',NULL,'2020-12-10','2020-12-10'),(74,13,'019ed0e07bdb483f6f3e8f7458751e3e',NULL,'2020-12-10','2020-12-10'),(75,6,'4eac104af6723bd8334b04568f730b5f',NULL,'2020-12-10','2020-12-10'),(76,8,'b9ec4caa152f24d2523b06e41fe30adf',NULL,'2020-12-10','2020-12-10'),(77,8,'d902c825cab2cb111447608509ac2190',NULL,'2020-12-10','2020-12-10'),(78,8,'edd7d5b4476dfec377be09f3de79ad2d',NULL,'2020-12-10','2020-12-10'),(79,8,'2ec42d8c879121236bbc788ccd8fe435',NULL,'2020-12-10','2020-12-10'),(80,6,'857b5c3621513a4ebd21d8fb6fd76b7c',NULL,'2020-12-10','2020-12-10'),(81,8,'7f8a29b4d3f27a10c4d6331b79c6d87d',NULL,'2020-12-10','2020-12-10'),(82,8,'bde0ca3fece49775f703a4892ba0c1d2',NULL,'2020-12-10','2020-12-10'),(83,6,'7258ce4b2db6e836c45bbce220267b02',NULL,'2020-12-10','2020-12-10'),(84,8,'57d843ed97717f12182bd09bec1f7d1f',NULL,'2020-12-10','2020-12-10'),(85,6,'9e7cf1feae2a11c4d4f3fd3143f4196e',NULL,'2020-12-10','2020-12-10'),(86,6,'2ca2a3bcdc2c9f9a008696176ee1fd74',NULL,'2020-12-10','2020-12-10'),(87,6,'5e2197f31959706c510e24dcb67140bc',NULL,'2020-12-11','2020-12-11'),(88,6,'a8731027246b50f2f46b67311ee63104',NULL,'2020-12-11','2020-12-11'),(89,13,'649ab7979e696e598b87e7a7f6bbc170',NULL,'2020-12-11','2020-12-11'),(90,12,'bcdb5d75fe7f13c11638f30a433e2a60',NULL,'2020-12-11','2020-12-11'),(91,12,'7dd4408673ace75da2527ce760d7f626',NULL,'2020-12-11','2020-12-11'),(92,8,'44fa6f4d0a868d9de9857755c84fd29e',NULL,'2020-12-14','2020-12-14'),(93,8,'318ac880bc06a61b2adaa791adfaed70',NULL,'2021-01-05','2021-01-05');
/*!40000 ALTER TABLE `mobilesessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userroles`
--

LOCK TABLES `userroles` WRITE;
/*!40000 ALTER TABLE `userroles` DISABLE KEYS */;
INSERT INTO `userroles` VALUES (1,'admin',1,'2019-10-14','2019-10-14'),(2,'user',2,'2019-10-14','2019-10-14');
/*!40000 ALTER TABLE `userroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `last_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `Column 11` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (15,'Mohsin','Tariq','mohsintariq.11@gmail.com','2c3VplWty6xQ.',1,NULL,1,'2020-12-09 13:41:48','2020-12-08 06:59:01',NULL),(2,'test','123','volox46877@tjuln.com','9e/6U4uWSFys2',2,NULL,0,'2020-12-09 11:39:34','2020-11-20 07:17:47',NULL),(7,'TEST USER','THESET USER','datido3830@bcpfm.com','17j/AQkpV6BbU',1,NULL,1,'2020-12-09 12:09:54','2020-11-24 09:28:40',NULL),(4,'TEST USER','THESET USER','camej27372@ummoh.com','2fDSbpQtjjVFc',2,NULL,0,'2020-11-24 07:01:29','2020-11-24 07:01:29',NULL),(6,'jack','biteng','jack.biteng@irsis.ph','6aleHPYZxGF7o',1,NULL,1,'2020-12-10 05:15:53','2020-11-24 09:13:28',NULL),(8,'testqwe','test','yifan.mayson@extraale.com','92AB9KTHr6zVY',1,NULL,1,'2020-11-24 10:06:50','2020-11-24 10:06:32',NULL),(9,'qwa','qaw','salmanali2225@extraale.com','39Ws9Bz89V54I',1,NULL,0,'2020-12-09 09:32:00','2020-11-24 10:49:43',NULL),(14,'TEST USER','THESET USER','rican42671@wncnw.com','b2dB8HilrVU5k',2,NULL,1,'2020-12-09 11:18:34','2020-12-07 09:58:12',NULL),(11,'Jack','Biteng','jack.biteng@topc.ph','37N91eKVUpVm.',2,NULL,1,'2020-11-24 11:05:13','2020-11-24 11:04:39',NULL),(12,'Jack','Biteng','pbiteng@fountainheadtechnologies.com','86yiTuy4kP/zI',2,NULL,1,'2020-12-09 11:36:12','2020-12-03 08:52:40',NULL),(13,'Brian Paul','Lizardo','lamuyan@gmail.com','c5wwS6bVBu3AY',2,NULL,1,'2020-12-09 11:38:04','2020-12-07 02:42:30',NULL),(16,'testi','user','nijese8485@menece.com','d0DDsQfEQPk/A',2,NULL,1,'2020-12-09 11:38:37','2020-12-08 17:32:35',NULL),(17,'Juanito','Buyer','buyer.juanito@gmail.com','c4geKFxcFdhRo',2,NULL,1,'2020-12-09 07:35:45','2020-12-09 07:35:04',NULL),(18,'support','irsis','support@irsis.ph','2c3VplWty6xQ.',1,NULL,1,'2020-12-09 13:00:53','2020-12-09 13:00:56',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-15 22:41:44
