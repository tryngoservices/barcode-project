<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
  	<p>{{ $messages }}</p>
  	<a href="{{ $url }}">{{ $url }}</a>
</body>
</html>