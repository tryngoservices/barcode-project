DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `first_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `last_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `email` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobilesessions`;
CREATE TABLE `mobilesessions` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `mobileemailotp`;
CREATE TABLE `mobileemailotp` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `user_id` int(11) NOT NULL,
  `tokenforemail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiry_date` datetime COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
    primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `userroles`;
CREATE TABLE `userroles` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `role_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `status` boolean COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "admin",
    1,
    "2019-10-14",
    "2019-10-14"
);
INSERT INTO `userroles`(
    `role_name`,
    `status`,
    `updated_at`,
    `created_at`
)
VALUES(
    "user",
    2,
    "2019-10-14",
    "2019-10-14"
);


DROP TABLE IF EXISTS `email_content`;
CREATE TABLE `email_content` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `template_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `subject` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `content` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `email_content`(
    `template_name`,
    `subject`,
    `content`
)
VALUES(
    "sign_up",
    "Sign Up",
    "Email Verification Code\nKindly Enter the Below Email Verification Code to verify your email\n"
),(
    "forgot_password",
    "Forgot Password",
    "Forget Password\nKindly Enter the Below Verification Code to verify it's You\n"
),(
    "send_report",
    "TrackrMobile Report",
    "You are getting this email because you requested for a report on your scanning activity.\nKindly click the link below:\n"
);

DROP TABLE IF EXISTS `connection_details`;
CREATE TABLE `connection_details` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `attribute_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `attrib_value` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `connection_details`(
    `attribute_name`,
    `attrib_value`
)
VALUES(
    "host_url",
    "https://trackrgamma.topc.ph"
),(
    "db",
    "trackrgamma"
),(
    "username",
    "dev"
),(
    "password",
    "dev"
),(
    "csv_path_absolute",
    "https://trackrmobile.topc.ph/"
);

DROP TABLE IF EXISTS `encryption_parameter`;
CREATE TABLE `encryption_parameter` (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `attribute_name` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `attrib_value` LONGTEXT COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `encryption_parameter`(
    `attribute_name`,
    `attrib_value`
)
VALUES(
    "mode",
    "CBC"
),(
    "key_size",
    "256"
),(
    "init_vector",
    "KadyaNuTet-ewaKa"
),(
    "secret_key",
    "s6v8y/B?E(H+MbQeThWmZq4t7w!z$C&F"
);