<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::post('mobilesignup', 'Api\MobileController@mobileSignUp');

Route::post('mobilelogin', 'Api\MobileController@mobilelogin');

Route::post('mobilelogout', 'Api\MobileController@mobilelogout');

Route::post('mobileusers', 'Api\MobileController@mobileUsers');

Route::post('mobileuseraccountupdate', 'Api\AdminController@changeUserDetail');

Route::post('useraccountactive', 'Api\MobileController@userAccountActive');

Route::post('adminuseraccount', 'Api\MobileController@makeAdminUserAccount');

Route::post('useraccountunactive', 'Api\MobileController@userAccountUnActive');

Route::post('useraccountdelete', 'Api\MobileController@userAccountDelete');


Route::post('emailverify', 'Api\MobileController@otpConfirmViaEmail');

/* Change/Reset Password */
Route::post('forgotpassword', 'Api\MobileController@forgetPassword');

Route::post('forgotpasswordotp', 'Api\MobileController@matchforgotpasswordCode');

Route::post('newpassword', 'Api\MobileController@newPassword');


Route::get('test', 'Api\AdminController@getRecord');
Route::get('client', 'Api\AdminController@getClient');
Route::post('insertscanhistory', 'Api\AdminController@makeScanHistoryInsert');
Route::post('makereportcsv', 'Api\AdminController@makeScanReport');

Route::get('odoo', 'Api\MobileController@odooshitcalling');

Route::post('table', 'Api\MobileController@checktable');

Route::get('queries', 'Api\MobileController@queryDB');
Route::get('queries', 'Api\MobileController@queryDB');


Route::post('updateconnectiondetails', 'Api\AdminController@updateConnectionFeature');
Route::post('getencryptparams', 'Api\AdminController@getEncrypParamsForMobile');
Route::post('updateencryptparamss', 'Api\AdminController@updateEncryptionParameters');
Route::post('updateemailtemp', 'Api\AdminController@updateEmailTemp');
Route::post('getconnectiondetailsmobile', 'Api\AdminController@getConnectionDetailForMobile');
Route::post('getallemailtemp', 'Api\AdminController@getAllEmailTemplate');